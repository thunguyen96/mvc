<?php
include_once("db/db.php");

class Model {

	public function __construct()  
     {  
          $this->db = new DB();
     }

	public function getUserList($data = [])
	{	
		if(isset($_REQUEST['search'])){
			$ma_sinhvien= isset($data["ma_sinhvien"]) ? $data["ma_sinhvien"] : '';
			$gender= isset($data["gender"]) ? $data["gender"] : '';
			$birthday= isset($data["birthday"]) ? $data["birthday"] : '';
			$email= isset($data["email"]) ? $data["email"] : '';
			if($ma_sinhvien==Null && $gender==Null && $birthday==Null && $email==Null){
				echo "Xin nhập dữ liệu";
			}else{

				$where =array();
			
				if($data){
			
					
					$where[]="gender='$gender'";
					

					if(isset($ma_sinhvien) && $ma_sinhvien){
						$where[]="ma_sinhvien='$ma_sinhvien'";
					}				

					if(isset($birthday) && $birthday){
						$where[]="birthday='$birthday'";
					}
					if(isset($email) && $email){
						$where[]="email='$email'";
					}

					$where=implode(' AND ', $where);

					if($where){
						$where = "where ".$where;
					}
				}
				if($where){
					$sql="select * from students ". $where;
				}
			}
		}
		else{
			$sql="select * from students";
		}
		return mysqli_query($this->db->connect(),$sql);
	}
		

	public function add($data=[]){
		if(isset($_POST['submit'])){

			if(($_POST['masv']=="") || ($_POST['lastname']=="") || ($_POST['firstname']=="") || ($_POST['birthday']=="") || ($_POST['phone']=="") 
				|| ($_POST['email']=="") || ($_POST['password']=="") || ($_POST['address']=="") || 
				($_POST['created']=="") || ($_POST['updated'])=="" || $_FILES['file']['name']==""){
		
				echo $error="Chua duoc nhap ";
			} else {
				$masv= $_POST['masv'];
				$lastname= $_POST['lastname'];
				$firstname = $_POST['firstname'];
				$birthday = $_POST['birthday'];
				$phone = $_POST['phone'];
				$email = $_POST['email'];
				$password = $_POST['password'];
				$address = $_POST['address'];
				$created = $_POST['created'];
				$updated = $_POST['updated'];
			if(isset($_POST['female'])){
				$gender=0;
			}else{
				$gender=1;
			}

			$type_img = $_FILES['file']['type'];
			$types = array(
				'image/jpeg',
				'image/png',
				'image/gif'
			);
			if(in_array($type_img, $types)){
				if($_FILES['file']['size'] > 10000000){
					echo "File không được lớn hơn 2MB";
				}else{
					$path = "avatar/"; 
	                $tmp_name = $_FILES['file']['tmp_name'];
	                $name = $_FILES['file']['name'];
	                $type = $_FILES['file']['type']; 
	                $size = $_FILES['file']['size']; 
	                $url=$path.$name;	

                // Upload file
                	move_uploaded_file($tmp_name,$path.$name);
                	$query1="INSERT INTO students(ma_sinhvien, last_name, first_name, gender, birthday, phone, email, password, address, avatar, created, updated) VALUES ('$masv','$lastname','$firstname','$gender','$birthday','$phone','$email','$password','$address','$url','$created','$updated')";
               		
               		if(mysqli_query($this->db->connect(),$query1)== TRUE ){
						echo "insert thanh cong!";
					}
				}
			
			}else{
				echo "Kieu file không hợp lệ ";
			}	
		}
	}
	}
	
	public function edit($id){
		if(isset($_REQUEST['id'])){
   	
   			if(isset($_POST['submit'])){
		   		$masv=$_POST['masv'];
		   		$last_name=$_POST['lastname'];
		   		$first_name=$_POST['firstname'];
		   		$birthday=$_POST['birthday'];
		   		$phone=$_POST['phone'];
		   		$email=$_POST['email'];
		   		$password=$_POST['password'];
		   		$address=$_POST['address'];
		   		$created=$_POST['created'];
		   		$updated=$_POST['updated'];
		   
		   		$type_img = $_FILES['file']['type'];
		   		$types = array(
		   			'image/jpeg',
		   			'image/png',
		   			'image/gif'
		   		);
   				if(in_array($type_img, $types)){
   					if($_FILES['file']['size'] > 10000000){
   						echo "File không được lớn hơn 2MB";
   					}else{
	   					$path =  'E:xampp/htdocs/mvc/avatar'; 
			             $tmp_name = $_FILES['file']['tmp_name'];
			             $name = $_FILES['file']['name'];
			             $type = $_FILES['file']['type']; 
			             $size = $_FILES['file']['size']; 
			             $url=$path.$name;	  
			           	 move_uploaded_file($tmp_name,$path.$name);                   	
   					}
   				}else{
   					echo "Kieu file không hợp lệ ";
	   			}	

	      		$sql="UPDATE students SET ma_sinhvien='$masv',last_name='$last_name',first_name='$first_name',birthday='$birthday',phone='$phone',email='$email',password='$password',address='$address',avatar='$url',created='$created',updated='$updated' WHERE id='".$_GET['id']."'";
	            $query=mysqli_query($this->db->connect(),$sql);
	            if($query== TRUE ){
	                         echo "Update thanh cong!";
	            }
   			}else{
   				if(isset($_REQUEST['id'])){
   					$sql1="SELECT * FROM students WHERE id='".$_REQUEST['id']."'";
      				$rowCollection = mysqli_query($this->db->connect(),$sql1);
     		 		while($row = mysqli_fetch_array($rowCollection)){
		       			$masv=$row['ma_sinhvien'];
		       			$last_name=$row['last_name'];
		       			$first_name=$row['first_name'];
		       			$gender=$row['gender'];
		       			$birthday=$row['birthday'];
		       			$phone=$row['phone'];
		       			$email=$row['email'];
		       			$password=$row['password'];
		       			$address=$row['address'];
		       			$created=$row['created'];
		       			$updated=$row['updated'];
		       			$url=$row['avatar'];
   
       				}
   				}
   			}
   		}
	}

	public function delete($id){
		if(isset($_GET['id'])){
			$id=$_GET['id'];
			$sql="SELECT id FROM students where id='$id'";
			$query=mysqli_query($this->db->connect(),$sql);
			if($query->num_rows>0){
				$sql1="DELETE FROM students where id='$id'";
				if(mysqli_query($this->db->connect(),$sql1)){
					echo "Xoa thanh cong!";
				}else{
				echo "Error";
				}
			}else{
				echo "ID khong ton tai";
			}	
		}else{
		echo "Ban chua nhap ID";
		}	
	}
}